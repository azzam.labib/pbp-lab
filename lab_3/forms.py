from django import forms
from lab_1.models import Friend

# thanks to https://www.geeksforgeeks.org/django-modelform-create-form-from-models/

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
