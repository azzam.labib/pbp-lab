from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required


# reference : https://realpython.com/django-redirects/
# https://docs.djangoproject.com/en/3.2/topics/auth/default/#authentication-in-web-requests
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    if(request.method == "POST"):
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab-3')

    else:
        form = FriendForm()

    return render(request, "lab3_form.html", {"form" : form})