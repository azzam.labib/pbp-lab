import 'package:flutter/material.dart';

import './models/toko.dart';

final DAFTAR_TOKO = [
  Toko(
    namaPerusahaan: "Azure Holding",
    namaToko: "Azure Choco",
  ),
  Toko(
    namaPerusahaan: "Mak Sasshi Group",
    namaToko: "なぎさのハート",
  ),
  Toko(
    namaPerusahaan: 'PT. Hard SDA by Hayate',
    namaToko: "なるちゃん の Hat",
  ),
];
