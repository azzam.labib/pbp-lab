from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(verbose_name="to",max_length=30)
    dari = models.CharField(verbose_name="from",max_length=30)
    title = models.CharField(verbose_name="title",max_length=30)
    message = models.CharField(verbose_name="message",max_length=1000)
