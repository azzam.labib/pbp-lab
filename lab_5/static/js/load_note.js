$(document).ready(function(){
    $.ajax({
        url: "/lab-2/json",
        success: function (results) {
            results.map(function(results){
                $(".tablebody").append(`
                <tr>
                <td>${results.fields.to}</td>
                <td>${results.fields.dari}</td>
                <td>${results.fields.title}</td>
                <td>
                    <button type="button" value="${results.pk}" class="btn btn-outline-dark btn-v" data-bs-toggle="modal" data-bs-target="#viewModal"> View </button>
                    <button type="button" class="btn btn-outline-dark btn-e" data-bs-toggle="modal" data-bs-target="#editModal"> Edit </button>
                    <button type="button" class="btn btn-outline-dark btn-d" data-bs-toggle="modal" data-bs-target="#deleteModal">Delete</button>
                </td>
            </tr>`);
            });
        },
    });
});