1. Apakah perbedaan antara JSON dan XML?
JSON adalah Javascript Object Notation (sehingga dibuat berdasarkan bahasa Javascript). SEdangkan XML adalah Extensible markup language yang diturunkan dari SGML.
JSON lebih menekankan pada cara merepresentasikan objek dibandingkan dengan XML yang memakai struktur tag untuk merepresentasikan data items. 

Selain itu, terdapat beberapa perbeaan secara teknis dari JSON dan XML, diantaranya:
JSON : tidak support namespaces, support array, filenya lebih mudah dibaca dibandingkan XML, lebih tidak aman, tidak support comments, dan hanya mendukung UTF-8 encoding.
XML : support namespaces, tidak support array, dokumennya lebih sulit dibaca, lebih aman dari JSON, support comments, dan bisa support banyak encoding.


2. Apakah perbedaan antara HTML dan XML?
HTML (Hyper Text Markup Language) adalah bahasa yang mengkombinasikan Markup Language dan Hypertext yang digunakan dalam pembuatan situs dan aplikasi web, sedangkan XML (extensive Markup Language) menyediakan frameworks untuk mendefinisikan markup languages dan juga digunakan dalam pembuatan situs dan aplikasi web.
Perbedaan paling mencolok dari kedua hal tersebut adalah, HTML digunakan untuk menampilkan data dan bukan untuk mentransfer data, sedangkan XML digunakan untuk mentranfer data tetapi tidak digunakan untuk menampilkan data.
HTML bersifat static sedangkan XML bersifat dinamis.

Perbedaan lain yang bersifat teknis diantaranya: 
HTML: dapat mengabaikan error kecil, not Case Sensitive, tagsnya predefined, banyaknya jenis tags terbatas.
XML: error tidak ditoleransi, Case Sensitive, tagsnya user defined, tagsnya extensible.
